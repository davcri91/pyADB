# Devices properties

Here you will find files containing the output of 
```
adb shell getprop
```
for various devices.
This is needed to understand how the properties' name is standardized.
