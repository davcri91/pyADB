# Open Droid Kit [(temporary name...)](https://gitlab.com/davcri91/pyADB/issues/15)

__OS__: Linux (Mac and Windows planned)  
__Programming Language__: Python  
__Type__: console-based  
__License__: gpl  
__Current State__: Prototype  

## What is it ?
A prototype of a console based application to simplify maintenance and modding of Android devices;
It can support, virtually, any android device because of its plugin system.

It’s intended for both users who want a simple way to install mods and modders
who want to release an official setup script for their work.

## Why ?
Because we can create official installation scripts with official requirements.
It will reduce trivial errors and modders will be 100% sure about what others did
in order to install its software (it's all documented in the script).

## Similar programs
The idea is to create a program similar to the Nexus Root Toolkit (http://www.wugfresh.com/nrt/).  
The main difference will be that Open Droid Toolkit can manage,
potentially, all Android smartphones and tablets with the plugin system.
Moreover OAT will be Open Source and cross-platform (Windows, Linux, MacOS),
unlike the NRT that runs only on Windows.  
Other similar programs:  
Android Ultimate Toolbox (http://forum.xda-developers.com/showthread.php?t=1886562)

# Running
You'll need Android SDK installed and ANDROID_PATH environment variable configured.
Download it from here: https://developer.android.com/sdk/installing/index.html?pkg=tools
Find  and run `tools/android` then download _Android SDK Platform-tools_.
Now you should set the ANDROID_PATH environment variable.

Now make executable and then run `./run.py`.  
You can run `sudo pip install -r requirements.txt` to install all dependencies, but
at the moment no external modules are used.


## Contributing
todo
