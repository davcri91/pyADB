import os
import subprocess

from entity.device import Device

class AndroidHelper:
	"""docstring for """


	def __init__(self):
		self.androidHome = os.getenv("ANDROID_HOME")

		# initializing variables containing the path to the binaries (adb and fastboot)
		if self.androidHome != None:
			platformTools = self.androidHome + "/platform-tools"
			self.adb = platformTools + "/adb"
			self.fastboot = platformTools + "/fastboot"

			if not self.verifyBinaries():
				print("error: adb or fastboot not found") # to log/stderr

		else:
			print("ANDROID_HOME is not set") # to log/stderr
			print("Checking if android tools are installed system-wide")
			self.adb = "adb"
			self.fastboot = "fastboot"

			if not self.verifyBinaries():
				print("error: adb or fastboot not found") # to log/stderr


	def verifyBinaries(self):
		# opening devnull to redirect the output of the following commands
		devnull = open(os.devnull)

		adbInstalled = False
		fastbootInstalled = False

		# When python 3.3 will be defaulted, this line should be used:
		# if subprocess.call([adb, "version"], subprocess.DEVNULL) == 0:
		if subprocess.call([self.adb, "version"], stdout=devnull) == 0:
		    adbInstalled = True;
		if subprocess.call([self.fastboot, "help"], stdout=devnull, stderr=devnull) == 0:
		    fastbootInstalled = True;

		devnull.close()

		return adbInstalled and fastbootInstalled

	def startAdbServer(self):
		subprocess.call([self.adb, "start-server"])

	def getDevices(self):
		# getting the output of "adb devices -l"
		output = subprocess.check_output([self.adb, "devices", "-l"], universal_newlines=True)
		output = output.splitlines() # return a list containing lines, also blank ones
		# print(output)

		# removing header
		del output[0]
		# removing blank lines from output
		deviceList = [line for line in output if line]

		return deviceList

	def getProp(self, prop):
		out = subprocess.check_output([self.adb, 'shell', 'getprop', prop],
										universal_newlines=True)
		# remove blank line
		out = out.rstrip()
		return out

	def isValid(self):
		out = subprocess.check_output([self.adb, 'get-state'], universal_newlines=True)

		# adb get-state returns "unknown" when:
		# - no device is connected
		# - 2 (or more) device are connected and usb debugging is active for both
		# - one device is connected but it doesn't aknowledged the authorization
		#	for usb debugging

		if out.rstrip() != 'unknown':
			valid = True
		else:
			valid = False
		return valid

	def rebootBootloader(self):
		subprocess.call([self.adb, 'reboot', 'bootloader'])

	def rebootRecovery(self):
		subprocess.call([self.adb, 'reboot', 'recovery'])

	# not completed !
	# from urllib.request import urlretrieve
	def download(self, url):
		print('Starting download')
		print('This could take some minutes')

		tmpName = 'TEST'
		local_filename, headers = urlretrieve(url, tmpName)
		print('FUNCTION NOT COMPLETED')
		# continue and...
		# return some data

	def test(self, deviceList):
		devicesInfo = []
		for dev in deviceList:
			model = ''
			device = ''
			for string in dev.split():
				[field, sep, value] = string.rpartition(':')

				if field == 'model':
					model = value

				if field == 'device':
					device = value
			devicesInfo.append([model, device])
