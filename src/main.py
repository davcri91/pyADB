import subprocess
import os
# import colorama # not used yet

from sys import stderr, stdout

from androidhelper import AndroidHelper
from xmlpluginparser import XmlPluginParser as Plugin

androidHelper = AndroidHelper()

# could take ~2 seconds if the server has not been started,
# otherwise it simply goes on (it doesn't start more than 1 server)
androidHelper.startAdbServer()

discoveredValidDevice = False
quit = False
while not discoveredValidDevice and not quit:
    devices = androidHelper.getDevices()
    attachedDevices = len(devices)

    if attachedDevices == 0:
       print('No device found')
       print('Have you activated USB debugging on your device ?')
       input('Press any key to continue')
    elif attachedDevices == 1:
        if androidHelper.isValid():
            discoveredValidDevice = True
        else:
            print('Device not recognized. Did you give authorizations for USB debugging?')
            input('Press any key to continue')
    else:
        print('Too many devices connected. Plug only the device you want to mod')
        input('Press any key to continue')

# FAKE VALUES
# brand = 'asus'
# model = 'tf300t'

try:
    brand = androidHelper.getProp('ro.product.brand').lower()
    model = androidHelper.getProp('ro.build.product').lower()
except Exception:
    # handle exception
    exit()

print('brand=', brand)
print('model=', model)
print()

# read plugins files and list the possible actions
try:
    pluginsDir = './plugins/' + brand + '/' + model
    actions = os.listdir(pluginsDir)

    # loading plugins
    plugins = []

    for i, a in enumerate(actions):
        p = Plugin(pluginsDir + '/' + a, a)
        p.parse()
        plugins.append(p)

    # printing available plugins/actions
    print('Actions for this device:')
    for i, p in enumerate(plugins):
        print(str(i+1) + ')', p)
    print('Press q to quit\n')

    # asking user which plugin to run
    pluginSelected = False
    quit = False

    while not quit:
        while not pluginSelected and not quit:
            opt = input('Select an option:\n>> ')

            if not opt.isdigit():
                if opt.lower() == 'q':
                    quit = True
                else:
                    print('Invalid input')
            elif int(opt) > len(actions) or int(opt) <= 0:
                print('Out of range')
            else:
                pluginSelected = True

        if not quit:
            selectedPlugin = plugins[int(opt)-1]
            print('You selected', selectedPlugin)
            print('Description:' + selectedPlugin.description, '\nAre you sure? [y/n]')
            ans = input('>> ')

            answerIsValid = False

            while not answerIsValid:
                if ans.lower() == 'y':
                    selectedPlugin.run()
                    answerIsValid = True
                    pluginSelected = False
                elif ans.lower() == 'n':
                    answerIsValid = True
                    pluginSelected = False
                    print('Exiting')
                else:
                    print('y -> yes, n -> no')
                    ans = input('>> ')
        else:
            print('Exiting...')
except OSError as e:
    print('No plugin found for your device')
    print(e) # this should go to sderr/log file
