from xmlpluginparser import XmlPluginParser

xmlPP = XmlPluginParser()
xmlPP.parse('plugin-samples/motorola/titan_umtsds/twrp/twrp.xml')

print(xmlPP)

# tree = ET.parse('plugin-samples/motorola/titan_umtsds/twrp/twrp.xml')
# root = tree.getroot()
#
# for child in root:
# 	if child.tag == "summary":
# 		summary = child.text.strip()
# 	if child.tag == "description":
# 		description = child.text.strip()
# 	if child.tag == "author":
# 		author = child.text.strip()
# 	if child.tag == "license":
# 		license = child.text.strip()
# 	if child.tag == "source":
# 		source = child.text.strip()
# 	if child.tag == "email":
# 		email = child.text.strip()
# 	if child.tag == "device":
# 		model = child.attrib["model"]
# 		vendor = child.attrib["vendor"]
# 	if child.tag == "requires":
# 		androidVersion = child.attrib["android-version"]
# 		bootloaderVersion = child.attrib["bootloader-version"]
# 		root = child.attrib["root"]
# 		unlockedBootloader = child.attrib["unlocked-bootloader"]
# 	if child.tag == "script":
# 		scriptFileName = child.attrib["file-name"]
#
# 		if child[0].tag == "url-file":
# 			varName = child[0].attrib["var-name"]
# 			downloadUrl = child[0].text.strip()
#
# print(summary)
# print(description)
# print(author)
# print(license)
# print(source)
# print(email, "\nDevice:")
# print(model)
# print(vendor)
# print('Requirements:')
# print(androidVersion)
# print(bootloaderVersion)
# print(root)
# print(unlockedBootloader)
# print(scriptFileName)
# print(varName, downloadUrl)
