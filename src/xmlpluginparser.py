import xml.etree.ElementTree as ET
import subprocess

import sys

# rename this class as Plugin or PluginLoader?
class XmlPluginParser:
	def __init__(self, path, pluginName):
		self.xmlPath = path + '/' + pluginName + '.xml'
		self.scriptPath = path + '/' + pluginName + '.py'

	def parse(self):
		tree = ET.parse(self.xmlPath)
		root = tree.getroot()

		for child in root:
			if child.tag == "summary":
				self.summary = child.text.strip()
			if child.tag == "description":
				self.description = child.text.strip()
			if child.tag == "author":
				self.author = child.text.strip()
			if child.tag == "license":
				self.license = child.text.strip()
			if child.tag == "source":
				self.source = child.text.strip()
			if child.tag == "email":
				self.email = child.text.strip()
			if child.tag == "device":
				self.model = child.attrib["model"]
				self.vendor = child.attrib["vendor"]
			if child.tag == "requires":
				self.androidVersion = child.attrib["android-version"]
				self.bootloaderVersion = child.attrib["bootloader-version"]
				self.root = child.attrib["root"]
				self.unlockedBootloader = child.attrib["unlocked-bootloader"]
			if child.tag == "script":
				self.scriptFileName = child.attrib["file-name"]

				if child[0].tag == "url-file":
					self.varName = child[0].attrib["var-name"]
					self.downloadUrl = child[0].text.strip()

	def __str__(self):
		out = self.summary
		# out += '\n' + self.description
		# out += '\n' + self.author
		# out += '\n' + self.license
		# out += '\n' + self.source
		return out

	def run(self):
		print('\n------Running plugin------\n')

		with open(self.scriptPath) as script:
			instructions = script.read() # instructions is a string
			exec(instructions)
			# print(instructions)

		print('\n------Done-------\n')
